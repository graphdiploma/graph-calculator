package info.bubna.graph.data.service.impl;

import com.google.gson.Gson;
import info.bubna.graph.data.config.ActiveMQConfig;
import info.bubna.graph.data.model.ComputationDTO;
import info.bubna.graph.data.model.ComputationResultDTO;
import info.bubna.graph.data.model.ComputationType;
import info.bubna.graph.data.service.IActiveMQService;
import info.bubna.graph.data.utils.ActiveMQConnectionConsumer;
import info.bubna.graph.data.utils.ActiveMQConnectionProducer;
import lombok.extern.slf4j.Slf4j;

import javax.jms.JMSException;
import java.util.Objects;

@Slf4j
public class ActiveMQService implements IActiveMQService {

    private final Gson gson;
    private final ActiveMQConfig cfg;

    private ActiveMQConnectionProducer producerConnection;
    private ActiveMQConnectionConsumer consumerConnection;

    public ActiveMQService(Gson gson, ActiveMQConfig cfg) {
        this.gson = gson;
        this.cfg = cfg;
        // Create a ConnectionFactory
        producerConnection = new ActiveMQConnectionProducer(
                cfg.getBrokerAddress(), cfg.getResultQueueName(), cfg.getLogin(), cfg.getPass()
        );
        consumerConnection = new ActiveMQConnectionConsumer(
                cfg.getBrokerAddress(), cfg.getCompQueueName(), cfg.getLogin(), cfg.getPass()
        );
    }

    @Override
    public void write(ComputationResultDTO computation) {
        var currSendRetries = 0;
        var sendingSuccess = false;
        while (currSendRetries < cfg.getSendRetryCount() && !sendingSuccess) {
            try {
                // Create a messages
                String text = gson.toJson(computation);
                producerConnection.createAndSendMessage(text, computation.getComputationType() == ComputationType.EOC ? 1 : 9);
                sendingSuccess = true;
            } catch (Exception e) {
                log.error("send error", e);
                try {
                    producerConnection.reconnect();
                } catch (Exception e1) {
                    log.error("reconnect failed", e1);
                }
                currSendRetries++;
            }
        }
        if (!sendingSuccess) throw new RuntimeException("cant't send message");
    }

    @Override
    public ComputationDTO read() {
        var currReceiveRetries = 0;
        var receiveSuccess = false;
        ComputationDTO result = null;
        while (currReceiveRetries < cfg.getReceiveRetryCount() && !receiveSuccess) {
            try {
                // Create a messages
                var msg = consumerConnection.consumeTextMessage();
                result = gson.fromJson(msg.getText(), ComputationDTO.class);
                receiveSuccess = true;
            } catch (Exception e) {
                log.error("receive error", e);
                try {
                    producerConnection.reconnect();
                } catch (Exception e1) {
                    log.error("reconnect failed", e1);
                }
                currReceiveRetries++;
            }
        }
        if (!receiveSuccess) throw new RuntimeException("cant't send message");
        return Objects.requireNonNull(result);
    }

    @Override
    public void close() throws JMSException {
        producerConnection.close();
        consumerConnection.close();
    }
}
