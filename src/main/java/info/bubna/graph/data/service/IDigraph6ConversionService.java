package info.bubna.graph.data.service;

public interface IDigraph6ConversionService {

    /**
     * Конвертация в матрицу смежности
     * @param digraph6 digraph6 кодированный ориентированый граф
     * @return матрица смежности исходного графа
     */
    Integer[][] toAdjMatrix(String digraph6);

    /**
     * Конвертация в digraph6
     * @param adjMatrix матрица смежности
     * @return digraph6
     */
    String toDigraph6(Integer[][] adjMatrix);
}
