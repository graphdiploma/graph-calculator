package info.bubna.graph.data.service.impl;

import info.bubna.graph.data.service.IDigraph6ConversionService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.LinkedList;

public class Digraph6ConversionService implements IDigraph6ConversionService {
    private static final Logger log = LoggerFactory.getLogger(Digraph6ConversionService.class);

    public Integer[][] toAdjMatrix(String digraph6) {
        log.trace(digraph6);
        var tmpDigraph6 = digraph6.substring(1);
        StringBuilder decodedAsciiDigraph6Str = new StringBuilder();
        var chars = tmpDigraph6.toCharArray();
        int verticesCount = chars[0] - 63;
        for (int j = 1; j < chars.length; j++) {
            var i = ((int) chars[j]) - 63;
            var bS = Integer.toBinaryString(i);
            var bRS = StringUtils.leftPad(bS, 6, '0');
            decodedAsciiDigraph6Str.append(bRS);
            log.trace("number - {}", bS);
            log.trace("binary number - {}", bRS);
        }
        var decodedStr = decodedAsciiDigraph6Str.toString();
        log.trace(decodedStr);
        char[] decodedStrChars = decodedStr.toCharArray();
        log.trace("decoded str chars - {}", new String(decodedStrChars));

        Integer[][] result = new Integer[verticesCount][];
        for (int i = 0; i < verticesCount; i++) {
            result[i] = new Integer[verticesCount];
            for (int j = 0; j < verticesCount; j++) {
                var c = Character.getNumericValue(decodedStrChars[i * verticesCount + j]);
                result[i][j] = c;
            }
        }
        return result;
    }

    @Override
    public String toDigraph6(Integer[][] inputMatrix) {
        var res = new StringBuilder();
        res.append('&');
        var verticesCount = inputMatrix.length;
        res.append((char) (63 + verticesCount));
        LinkedList<Integer> adjMatrix = new LinkedList<>();
        for (int i = 0; i < verticesCount; i++)
            adjMatrix.addAll(Arrays.asList(inputMatrix[i]).subList(0, verticesCount));
        var neededLength = Math.ceil(((float) adjMatrix.size()) / 6f) * 6;
        var addZerosCount = neededLength - adjMatrix.size();
        for (int i = 0; i <= addZerosCount; i++) adjMatrix.add(0);
        var s = new StringBuilder();
        for (Integer i : adjMatrix) {
            s.append(i);
            if (s.length() == 6) {
                var t = s.toString();
                res.append((char) (63 + Integer.parseInt(t, 2)));
                s = new StringBuilder();
            }
        }
        return res.toString();
    }
}
