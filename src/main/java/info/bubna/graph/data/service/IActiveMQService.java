package info.bubna.graph.data.service;

import info.bubna.graph.data.model.ComputationDTO;
import info.bubna.graph.data.model.ComputationResultDTO;
import org.apache.activemq.Closeable;

public interface IActiveMQService extends Closeable {
    /**
     * Отправить вычисления
     * @param computation единица вычисления
     */
    void write(ComputationResultDTO computation);

    /**
     * Получить результат вычислений
     * @return единица результата вычислений
     */
    ComputationDTO read();
}
