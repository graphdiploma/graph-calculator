package info.bubna.graph.data.service.impl;

import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;

public class CalculationService {

    @SuppressWarnings("ALL")
    private Pair<ArrayList<Vertex>, Integer> matrixDescription(int verticesCount, Integer[][] adjMatrix) {
        ArrayList<Vertex> vertices = new ArrayList<>();
        int edgesCount = 0;

        for (int i = 0; i < verticesCount; i++) {
            Vertex v = new Vertex();
            v.num = i;
            vertices.add(v);
        }

        for (int i = 0; i < verticesCount; i++)
            for (int j = 0; j < verticesCount; j++) {
                if (adjMatrix[i][j] == 1) {
                    vertices.get(i).out++;
                    vertices.get(j).in++;
                    edgesCount++;
                }
            }

        return Pair.of(vertices, edgesCount);
    }

    public boolean checkEdgesExtension(
            Integer[][] sourceAdjMatrix,
            Integer[][] targetAdjMatrix,
            int kCurrent
    ) {
        var sVerticesCount = sourceAdjMatrix.length;
        var targetMatrixDesc = matrixDescription(targetAdjMatrix.length, targetAdjMatrix);
        var sourceMatrixDesc = matrixDescription(sVerticesCount, sourceAdjMatrix);
        return checkVertexDegreeSum(targetMatrixDesc, kCurrent, sVerticesCount, false)
                && checkEdgesEmbedded(sourceAdjMatrix, targetAdjMatrix, sourceMatrixDesc, sVerticesCount);
    }
    /**
     * Проверка: targetAdjMatrix является мр-1р для sourceAdjMatrix
     *
     * @param sourceAdjMatrix           исходная матрица
     * @param targetAdjMatrix           целевая матрица
     * @param sourceVerticesDescription описание вершин исходного графа
     * @param sourceVerticesCount       количество вершин исходного графа
     * @return targetAdjMatrix является мр-1р для sourceAdjMatrix
     */
    @SuppressWarnings("ALL")
    private boolean checkEdgesEmbedded(
            Integer[][] sourceAdjMatrix,
            Integer[][] targetAdjMatrix,
            Pair<ArrayList<Vertex>, Integer> sourceVerticesDescription,
            Integer sourceVerticesCount
    ) {
        int k = 0;
        for (int i = 0; i < sourceVerticesCount; i++)
            for (int j = 0; j < sourceVerticesCount; j++)
                if (targetAdjMatrix[i][j] == 1)
                    k++;

        // Trying to embed m in each m2 (m2 = m1 with 1 edge deleted)
        for (int count = 0; count < k; count++) {
            ArrayList<Vertex> vertices = new ArrayList<>();
            int m_out_c = 0;

            for (int i = 0; i < sourceVerticesCount; i++) {
                Vertex v = new Vertex();
                v.num = i;
                vertices.add(v);
            }

            int k1 = 0;
            Integer[][] m2 = new Integer[sourceVerticesCount][];
            for (int i = 0; i < sourceVerticesCount; i++) {
                m2[i] = new Integer[sourceVerticesCount];
                for (int j = 0; j < sourceVerticesCount; j++)
                    if (targetAdjMatrix[i][j] == 1) {
                        if (k1 == count)
                            m2[i][j] = 0;
                        else {
                            m2[i][j] = 1;
                            vertices.get(i).out++;
                            vertices.get(j).in++;
                            m_out_c++;
                        }

                        k1++;
                    } else
                        m2[i][j] = 0;
            }

            Pair<ArrayList<Vertex>, Integer> p = Pair.of(vertices, m_out_c);
            Boolean check = checkMatricesEmbedded(sourceAdjMatrix, m2, sourceVerticesDescription, p, sourceVerticesCount, false);

            if (!check)
                return false;
        }

        return true;
    }

    public boolean checkVerticesExtension(
            Integer[][] sourceAdjMatrix,
            Integer[][] targetAdjMatrix,
            int kCurrent
    ) {
        var sVerticesCount = sourceAdjMatrix.length;
        var targetMatrixDesc = matrixDescription(targetAdjMatrix.length, targetAdjMatrix);
        var sourceMatrixDesc = matrixDescription(sVerticesCount, sourceAdjMatrix);
        return checkVertexDegreeSum(targetMatrixDesc, kCurrent, sVerticesCount, true)
                && checkVertexEmbedded(sourceAdjMatrix, targetAdjMatrix, sourceMatrixDesc, sVerticesCount);
    }

    /**
     * Проверка: targetAdjMatrix является мв-1р для sourceAdjMatrix
     *
     * @param sourceAdjMatrix           исходная матрица
     * @param targetAdjMatrix           целевая матрица
     * @param sourceVerticesDescription описание вершин исходного графа
     * @param sourceVerticesCount       количество вершин исходного графа
     * @return targetAdjMatrix является мв-1р для sourceAdjMatrix
     */
    @SuppressWarnings("ALL")
    private boolean checkVertexEmbedded(
            Integer[][] sourceAdjMatrix,
            Integer[][] targetAdjMatrix,
            Pair<ArrayList<Vertex>, Integer> sourceVerticesDescription,
            Integer sourceVerticesCount
    ) {
        // Remember targetAdjMatrix is sourceVerticesCount+1 size
        for (int count = 0; count < sourceVerticesCount + 1; count++) {
            ArrayList<Vertex> vertices = new ArrayList<>();
            int m_out_c = 0;

            for (int i = 0; i < sourceVerticesCount; i++) {
                Vertex v = new Vertex();
                v.num = i;
                vertices.add(v);
            }

            int i = 0, j = 0;
            Integer[][] m2 = new Integer[sourceVerticesCount][];
            for (int i1 = 0; i1 < sourceVerticesCount + 1; i1++)
                if (i1 != count) {
                    m2[i] = new Integer[sourceVerticesCount];
                    j = 0;
                    for (int j1 = 0; j1 < sourceVerticesCount + 1; j1++)
                        if (j1 != count) {
                            m2[i][j] = targetAdjMatrix[i1][j1];
                            if (targetAdjMatrix[i1][j1] == 1) {
                                vertices.get(i).out++;
                                vertices.get(j).in++;
                                m_out_c++;
                            }
                            j++;
                        }
                    i++;
                }

            Pair<ArrayList<Vertex>, Integer> p = Pair.of(vertices, m_out_c);
            boolean check = checkMatricesEmbedded(sourceAdjMatrix, m2, sourceVerticesDescription, p, sourceVerticesCount, false);

            if (!check) return false;
        }

        return true;
    }

    private boolean checkVertexDegreeSum(Pair<ArrayList<Vertex>, Integer> sourceMatrixDescription, int k, int verticesCount, boolean isVerticesExt) {
        for (var it : sourceMatrixDescription.getLeft()) {
            var vertexDegree = it.in + it.out;
            if ((isVerticesExt && vertexDegree > k) || (verticesCount > 5 && vertexDegree < 3) || vertexDegree > 4)
                return false;
        }
        return true;
    }

    public boolean checkIsomorphism(
            Integer[][] sourceAdjMatrix,
            Integer[][] targetAdjMatrix
    ) {
        var sDesc = matrixDescription(sourceAdjMatrix.length, sourceAdjMatrix);
        var tDesc = matrixDescription(targetAdjMatrix.length, targetAdjMatrix);

        return checkMatricesEmbedded(sourceAdjMatrix, targetAdjMatrix, sDesc, tDesc, sourceAdjMatrix.length, true);
    }

    /**
     * Проверка возможности отображения исходной матрицы в целевую
     *
     * @param sourceAdjMatrix           исходная матрица
     * @param targetAdjMatrix           целевая матрица
     * @param sourceVerticesDescription описание вершин исходного графа (sourceAdjMatrix)
     * @param targetVerticesDescription описание вершин целевого графа (targetAdjMatrix)
     * @param verticesCount             количество вершин исходного графа
     * @return Возможно ли вложить sourceAdjMatrix в targetAdjMatrix
     */
    private boolean checkMatricesEmbedded(
            Integer[][] sourceAdjMatrix,
            Integer[][] targetAdjMatrix,
            Pair<ArrayList<Vertex>, Integer> sourceVerticesDescription,
            Pair<ArrayList<Vertex>, Integer> targetVerticesDescription,
            Integer verticesCount,
            Boolean isIsomorphism
    ) {
        if ((isIsomorphism && !sourceVerticesDescription.getRight().equals(targetVerticesDescription.getRight()))
                || (!isIsomorphism && sourceVerticesDescription.getRight() > targetVerticesDescription.getRight()))
            return false;

        if (isIsomorphism) {
            int[] m_out = new int[verticesCount], m1_out = new int[verticesCount]; // Vector of d_out of vertices from 0 to n - 1
            for (int i = 0; i < verticesCount; i++) {
                m_out[sourceVerticesDescription.getLeft().get(i).out]++;
                m1_out[targetVerticesDescription.getLeft().get(i).out]++;
            }

            for (int i = 0; i < verticesCount; i++) {
                if (m_out[i] != m1_out[i])
                    return false;
            }
        }

        ArrayList<ArrayList<Integer>> potentialVertices = new ArrayList<>(); // For each sourceVerticesDescription.first vertex collect a list of vertices from targetVerticesDescription.first with which it can be matched
        for (int i = 0; i < verticesCount; i++) {
            potentialVertices.add(new ArrayList<>());
            for (int j = 0; j < verticesCount; j++) {
                var sourceVertexI = sourceVerticesDescription.getLeft().get(i);
                var targetVertexJ = targetVerticesDescription.getLeft().get(j);
                if ((sourceVertexI.in <= targetVertexJ.in) & (sourceVertexI.out <= targetVertexJ.out))
                    potentialVertices.get(i).add(j);
            }
        }

        for (int i = 0; i < verticesCount; i++) if (potentialVertices.get(i).size() == 0) return false;

        ArrayList<Integer> ans = new ArrayList<>();
        return checkVerticesMapping(sourceAdjMatrix, targetAdjMatrix, potentialVertices, 0, ans, isIsomorphism);
    }

    /**
     * Проверка возможности отображения исходной матрицы в целевую
     *
     * @param sourceAdjMatrix    исходная матрица
     * @param targetAdjMatrix    целевая матрица
     * @param potentialVertices  матрица, в которой
     *                           корневой индекс - номер вершины из sourceAdjMatrix,
     *                           вложенный массив Integer - номера вершин из targetAdjMatrix
     * @param vertexIndex        текущий номер вершины в исходной матрице
     * @param resVerticesIndeces массив номеров вершин targetAdjMatrix, в которые вкладываются вершины из sourceAdjMatrix
     * @return Возможно ли вложить sourceAdjMatrix в targetAdjMatrix
     */
    private boolean checkVerticesMapping(
            Integer[][] sourceAdjMatrix,
            Integer[][] targetAdjMatrix,
            ArrayList<ArrayList<Integer>> potentialVertices,
            Integer vertexIndex,
            ArrayList<Integer> resVerticesIndeces,
            Boolean isIsomorphism
    ) {
        for (int count = 0; count < potentialVertices.get(vertexIndex).size(); count++) {
            var a = potentialVertices.get(vertexIndex).get(count);
            if (!resVerticesIndeces.contains(a)) { // This vertex wasn't used yet
                resVerticesIndeces.add(a);

                // Checking edges between mapped vertices
                if (vertexIndex >= 1) {
                    boolean check = true;
                    for (int i = 0; i < resVerticesIndeces.size() - 1; i++) {
                        for (int j = i + 1; j < resVerticesIndeces.size(); j++) {
                            var ansI = resVerticesIndeces.get(i);
                            var ansJ = resVerticesIndeces.get(j);
                            if ((isIsomorphism && (!sourceAdjMatrix[i][j].equals(targetAdjMatrix[ansI][ansJ])
                                    || !sourceAdjMatrix[j][i].equals(targetAdjMatrix[ansJ][ansI])))
                                    || (!isIsomorphism && (sourceAdjMatrix[i][j] > targetAdjMatrix[ansI][ansJ]
                                    || sourceAdjMatrix[j][i] > targetAdjMatrix[ansJ][ansI]))) {
                                resVerticesIndeces.remove(resVerticesIndeces.size() - 1);
                                check = false;
                                break;
                            }
                        }
                        if (!check) break;
                    }

                    if (!check) continue;
                }

                if (vertexIndex == potentialVertices.size() - 1) return true;
                else {
                    if (checkVerticesMapping(
                            sourceAdjMatrix,
                            targetAdjMatrix,
                            potentialVertices,
                            vertexIndex + 1,
                            resVerticesIndeces,
                            isIsomorphism)
                    ) return true;
                    else
                        resVerticesIndeces.remove(resVerticesIndeces.size() - 1);
                }
            }
        }

        return false;
    }

    public static class Vertex {
        public Integer num;//TODO remove
        public Integer out = 0;
        public Integer in = 0;
    }
}
