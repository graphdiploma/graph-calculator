package info.bubna.graph.data.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Value;
import lombok.experimental.SuperBuilder;

import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Data
@SuperBuilder
public class ComputationResultDTO extends ComputationDTO {
    private Boolean result;
}
