package info.bubna.graph.data.model;

import lombok.Data;
import lombok.experimental.SuperBuilder;

import java.util.UUID;

@Data
@SuperBuilder
public class ComputationDTO {
    protected UUID id;
    protected ComputationType computationType;
    protected String digraph6Source;
    protected String digraph6Target;
    protected Integer kCurrent;
    protected Integer kMax;
}
