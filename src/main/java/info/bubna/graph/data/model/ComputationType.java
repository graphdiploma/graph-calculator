package info.bubna.graph.data.model;

public enum ComputationType {
    ISOMORPHISM,
    EDGES_EMBEDDED,
    VERTICES_EMBEDDED,
    EOC
}
