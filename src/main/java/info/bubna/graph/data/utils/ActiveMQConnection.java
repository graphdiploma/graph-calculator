package info.bubna.graph.data.utils;

import info.bubna.graph.data.exception.InitException;
import lombok.extern.slf4j.Slf4j;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.ActiveMQPrefetchPolicy;
import org.apache.activemq.Closeable;
import org.apache.commons.lang3.StringUtils;

import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Session;

@Slf4j
abstract class ActiveMQConnection implements Closeable {

    protected final String host;
    protected final String queue;
    protected final String login;
    protected final String pass;

    protected Connection connection;
    protected Session session;
    protected Destination destination;

    public ActiveMQConnection(String host, String queue, String login, String pass) {
        this.host = host;
        this.queue = queue;
        this.login = login;
        this.pass = pass;

        try {
            open();
        } catch (JMSException e) {
            log.error("init error;", e);
            throw new InitException(e);
        }
    }

    protected void open() throws JMSException {
        ActiveMQConnectionFactory connectionFactory;
        if (StringUtils.isAllEmpty(login, pass))
            connectionFactory = new ActiveMQConnectionFactory(host);
        else
            connectionFactory = new ActiveMQConnectionFactory(login, pass, host);
//        connectionFactory.setOptimizeAcknowledge(true);
        var prefetchConsumePolicy = new ActiveMQPrefetchPolicy();
        prefetchConsumePolicy.setQueuePrefetch(1);
        connectionFactory.setPrefetchPolicy(prefetchConsumePolicy);
        connectionFactory.setAlwaysSessionAsync(false);
        connectionFactory.setDispatchAsync(false);
        connection = connectionFactory.createConnection();

        connection.start();

        // Create a Session
        session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

        // Create the destination (Topic or Queue)
        destination = session.createQueue(queue);
    }

    public void reconnect() throws JMSException {
        close();
        open();
    }

    @Override
    public void close() throws JMSException {
        session.close();
        connection.close();
    }
}
