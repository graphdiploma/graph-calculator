package info.bubna.graph.data;


import com.google.gson.Gson;
import info.bubna.graph.data.config.ActiveMQConfig;
import info.bubna.graph.data.model.ComputationResultDTO;
import info.bubna.graph.data.model.ComputationType;
import info.bubna.graph.data.service.IActiveMQService;
import info.bubna.graph.data.service.IDigraph6ConversionService;
import info.bubna.graph.data.service.impl.ActiveMQService;
import info.bubna.graph.data.service.impl.CalculationService;
import info.bubna.graph.data.service.impl.Digraph6ConversionService;

public class Main {

    public static final Gson gson = new Gson();
    public static final ActiveMQConfig mqConfig = new ActiveMQConfig();
    public static final IActiveMQService mqService = new ActiveMQService(gson, mqConfig);
    public static final IDigraph6ConversionService conversionService = new Digraph6ConversionService();
    public static final CalculationService calcService = new CalculationService();

    /**
     * Unit tests the {@code GraphGenerator} library.
     *
     * @param args the command-line arguments
     */
    public static void main(String[] args) {
        while (true) {
            var computation = mqService.read();
            if (computation.getComputationType() != ComputationType.EOC) {
                boolean result = false;
                switch (computation.getComputationType()) {
                    case ISOMORPHISM:
                        result = calcService.checkIsomorphism(
                                conversionService.toAdjMatrix(computation.getDigraph6Source()),
                                conversionService.toAdjMatrix(computation.getDigraph6Target())
                        );
                        break;
                    case EDGES_EMBEDDED:
                        result = calcService.checkEdgesExtension(
                                conversionService.toAdjMatrix(computation.getDigraph6Source()),
                                conversionService.toAdjMatrix(computation.getDigraph6Target()),
                                computation.getKCurrent()
                        );
                        break;
                    case VERTICES_EMBEDDED:
                        result = calcService.checkVerticesExtension(
                                conversionService.toAdjMatrix(computation.getDigraph6Source()),
                                conversionService.toAdjMatrix(computation.getDigraph6Target()),
                                computation.getKCurrent()
                        );
                }
                mqService.write(ComputationResultDTO.builder()
                        .id(computation.getId())
                        .computationType(computation.getComputationType())
                        .digraph6Target(computation.getDigraph6Target())
                        .digraph6Source(computation.getDigraph6Source())
                        .kCurrent(computation.getKCurrent())
                        .kMax(computation.getKMax())
                        .result(result)
                        .build());
            } else {
                mqService.write(ComputationResultDTO.builder()
                        .id(computation.getId())
                        .computationType(computation.getComputationType())
                        .build());
            }
        }
    }
}
