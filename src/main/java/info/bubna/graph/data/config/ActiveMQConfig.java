package info.bubna.graph.data.config;

import info.bubna.graph.data.exception.InitException;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.ex.ConfigurationException;

import java.io.File;

@Slf4j
@Value
public class ActiveMQConfig {

    private final String brokerAddress;

    private final String login;
    private final String pass;

    private final String compQueueName;

    private final String resultQueueName;

    private final Integer sendRetryCount;

    private final Integer receiveRetryCount;

    public ActiveMQConfig() {
        Configurations configs = new Configurations();
        try {
            Configuration config = configs.properties(new File("mq.properties"));
            brokerAddress = config.getString("mq.address");
            compQueueName = config.getString("mq.queue.computation");
            login = config.getString("mq.login");
            pass = config.getString("mq.pass");
            resultQueueName = config.getString("mq.queue.result");
            sendRetryCount = config.getInt("mq.retry.send");
            receiveRetryCount = config.getInt("mq.retry.receive");
            // access configuration properties
        } catch (ConfigurationException cex) {
            log.error("init error;", cex);
            throw new InitException(cex);
        }
    }
}
