#!/bin/bash
export MQ_QUEUE_RESULT=from-computation
export DB_LOGIN=postgres
export DB_PASS=
export DB_URL=jdbc:postgresql://localhost:5432/cycle
export MQ_QUEUE_COMPUTATION=for-computation
export MQ_ADDRESS=tcp://localhost:61616
export MQ_LOGIN=
export MQ_PASS=
./gradlew run
